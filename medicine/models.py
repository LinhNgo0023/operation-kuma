from django.db import models
from django.conf import settings


# Create your models here.

class Medicine(models.Model):
    name = models.CharField(max_length=200)
    currently_taking = models.BooleanField(default=False)
    start_date = models.DateTimeField()
    quantity = models.DecimalField(max_digits=10, decimal_places=3)
    frequency = models.DecimalField(max_digits=10, decimal_places=3)
    description = models.TextField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="medicine",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name


#end date, if no end date display currently taking

class Basic_stats(models.Model):
    date = models.DateTimeField()
    height = models.DecimalField(max_digits=10, decimal_places=3)
    weight = models.DecimalField(max_digits=10, decimal_places=3)
    neck = models.DecimalField(max_digits=10, decimal_places=3)
    bust = models.DecimalField(max_digits=10, decimal_places=3)
    waist = models.DecimalField(max_digits=10, decimal_places=3)
    hip = models.DecimalField(max_digits=10, decimal_places=3)
    right_arm = models.DecimalField(max_digits=10, decimal_places=3)
    left_arm = models.DecimalField(max_digits=10, decimal_places=3)
    right_thigh = models.DecimalField(max_digits=10, decimal_places=3)
    left_thigh = models.DecimalField(max_digits=10, decimal_places=3)
    right_calf = models.DecimalField(max_digits=10, decimal_places=3)
    left_calf = models.DecimalField(max_digits=10, decimal_places=3)
    right_wrist = models.DecimalField(max_digits=10, decimal_places=3)
    left_wrist = models.DecimalField(max_digits=10, decimal_places=3)


#height two boxes, feet, in, or cm


#in/cm conversion
#weight, height, neck, bust, waist, hips, R.L arm (thickest part),
# R.L thigh, R.L calf, wrist,
#calculates bmi?


###add or goes into its own app directory and models.py?
# class Immunization_record(models.Model):
# class Basic_stats(models.Model):
# class Transfer_info(models.Model):
# class Discount_programs --->(no user input?)



###########
# class Schedule(models.Model):
#     doctor=models.ForeignKey(Doctor)
#     open=models.TimeField()
#     close=models.TimeField()

# class Appointment(models.Model):
#     """Contains info about appointment"""

#     class Meta:
#         unique_together = ('doctor', 'date', 'timeslot')

#     TIMESLOT_LIST = (
#         (0, '09:00 – 09:30'),
#         (1, '10:00 – 10:30'),
#         (2, '11:00 – 11:30'),
#         (3, '12:00 – 12:30'),
#         (4, '13:00 – 13:30'),
#         (5, '14:00 – 14:30'),
#         (6, '15:00 – 15:30'),
#         (7, '16:00 – 16:30'),
#         (8, '17:00 – 17:30'),
#     )

#     doctor = models.ForeignKey('Doctor',on_delete = models.CASCADE)
#     date = models.DateField(help_text="YYYY-MM-DD")
#     timeslot = models.IntegerField(choices=TIMESLOT_LIST)
#     patient_name = models.CharField(max_length=60)

#     def __str__(self):
#         return '{} {} {}. Patient: {}'.format(self.date, self.time, self.doctor, self.patient_name)

#     @property
#     def time(self):
#         return self.TIMESLOT_LIST[self.timeslot][1]
