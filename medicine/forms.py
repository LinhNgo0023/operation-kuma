from dataclasses import field
from django import forms
from medicine.models import Medicine, Basic_stats

class MedicineForm(forms.ModelForm):
    class Meta:
        model = Medicine
        fields = [
            "name",
            "currently_taking",
            "start_date",
            "quantity",
            "frequency",
            "description",
            "owner",
        ]


class Basic_statsForm(forms.ModelForm):
    class Meta:
        model = Basic_stats
        fields = [
            "date",
            "height",
            "weight",
            "neck",
            "bust",
            "waist",
            "hip",
            "right arm",
            "left arm",
            "right thigh",
            "left thigh",
            "right calf",
            "left calf",
            "right wrist",
            "left wrist",
        ]
