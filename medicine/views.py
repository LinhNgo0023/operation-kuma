from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from medicine.models import Medicine
from medicine.forms import MedicineForm

# Create your views here.
@login_required
def list_medication(request):
    medication = Medicine.objects.filter(owner=request.user)
    context = {
        "medicine_list": medication,
    }
    return render(request, "medicine/index.html", context)

@login_required
def show_medicine(request, id):
    medication = get_object_or_404(Medicine, id=id)
    context = {
        "medicine_object": medication,
    }
    return render(request, "medicine/detail.html", context)

@login_required
def create_medicine(request):
    if request.method == "POST":
        form = MedicineForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_medication")
    else:
        form = MedicineForm()
    context = {
        "form": form,
    }
    return render(request, "medicine/create.html", context)
