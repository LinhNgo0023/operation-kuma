from django.contrib import admin
from medicine.models import Medicine, Basic_stats
# Register your models here.

@admin.register(Medicine)
class MedicineAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "currently_taking",
        "start_date",
        "quantity",
        "frequency",
        "description",
        "owner",
    )


@admin.register(Basic_stats)
class Basic_statsAdmin(admin.ModelAdmin):
    list_display = (
        "date",
        "height",
        "weight",
        "neck",
        "bust",
        "waist",
        "hip",
        "right_arm",
        "left_arm",
        "right_thigh",
        "left_thigh",
        "right_calf",
        "left_calf",
        "right_wrist",
        "left_wrist",
    )
